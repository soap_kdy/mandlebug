﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnamyWeapon : MonoBehaviour
{
    public Transform enamyFirePont;
    public GameObject bigBlast_tranb;
    public float firerRate;
    private float nextShootTime;
    

    // Update is called once per frame
    void Update()
    {
        if (Time.time>nextShootTime)
        {
            Shoot();
            nextShootTime = Time.time + firerRate;
        }
    }

    void Shoot()
    {
        Instantiate(bigBlast_tranb, enamyFirePont.position, enamyFirePont.rotation);
    }



}

