﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistroyOutOfBounds : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // this is easy way for me to despawn the enamy blaster when that leave the screen 
    void Update()
    {
        if (transform.position.x > 10)
        {
            Destroy(gameObject);
        }

        if (transform.position.y > 10)
        {
            Destroy(gameObject);
        }

        if (transform.position.x < -10)
        {
            Destroy(gameObject);
        }

        if (transform.position.y < -10)
        {
            Destroy(gameObject);
        }
    }

}
