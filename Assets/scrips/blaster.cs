﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blaster : MonoBehaviour
{
    public float speed = 20f;
    public Rigidbody2D rb;
    public int damage = 40;
    // Start is called before the first frame update
    void Start()
    {
        rb.velocity = transform.right * speed;
    }
    //damage taken when the player hits the enamay
    private void OnTriggerEnter2D(Collider2D hitInfo)
    {
        EnamyHealth enemy = hitInfo.GetComponent<EnamyHealth>();
        if (enemy != null)
        {
            enemy.TakeDamage(damage);
            
        }

        Destroy(gameObject);
        Score.scoreValue += 10;
    }


}   
