﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float horizontalInput;
    public float speed = 10.0f;
    public float ShipSpeed = 15.0f;
    public Transform playerpoint;
    public float Upinput;
    public float minBound;
    public float maxBound;

    




    // Start is called before the first frame update
    void Start()
    {

    }

    // player movements with in a clockwise or counter clockwise path
    void Update()
    {
        {
            horizontalInput = Input.GetAxis("Horizontal");
            playerpoint.Rotate(0, 0, horizontalInput * Time.deltaTime * speed);

            Upinput = Input.GetAxis("Vertical");

            //if we are moving forwaord and  not too close then move. 
            float distanceFromCenter = transform.position.magnitude;
            if (Upinput > 0 && distanceFromCenter >= minBound)
                transform.Translate(Vector3.up * Upinput * Time.deltaTime * ShipSpeed);
            // otherwise if backwords and not too far them move backwards
            else if (Upinput < 0 && distanceFromCenter <= maxBound)
                transform.Translate(Vector3.up * Upinput * Time.deltaTime * ShipSpeed);

        }

        
        }
    }

    
    

