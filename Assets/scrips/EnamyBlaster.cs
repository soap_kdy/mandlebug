﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnamyBlaster : MonoBehaviour
{
    private PlayerHealth ship;

    public float speed = 20f;
    public Rigidbody2D rb;
    public int damage = 40;
    
    //the player takes on damage from the enamy
    void Start()
    {
        rb.velocity = transform.right * speed;

        ship = GameObject.FindGameObjectWithTag("ship").GetComponent<PlayerHealth>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("ship"))
            {
            ship.Damage(1);
            

        }
        Destroy(gameObject);
        
    }

}
