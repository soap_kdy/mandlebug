﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnamyHealth : MonoBehaviour
{
    public int health = 100;
    public GameObject deathEffect;

   
    public void TakeDamage (int damage)
    { //the enamy flashes red when hit by the player
        health -= damage;
        gameObject.GetComponent<Animation>().Play("enamyFlashDamage");

        if (health <= 0)
        {
            Die();
            Score.scoreValue += 10;
        }
    }

   
    void Die()
    {
        Instantiate (deathEffect, transform.position, Quaternion.identity);
        Destroy(gameObject);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 2);
    }
}
