﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enamyweapon1 : MonoBehaviour
{
    public Transform enamyFirePont1;
    public GameObject bigBlast_tranb;
    public float firerRate;
    private float nextShootTime;


    // for the enamy to shoot on its own and with a adjustbal shoot rate
    void Update()
    {
        if (Time.time > nextShootTime)
        {
            Shoot();
            nextShootTime = Time.time + firerRate;
        }
    }

    void Shoot()
    {
        Instantiate(bigBlast_tranb, enamyFirePont1.position, enamyFirePont1.rotation);
    }



}
