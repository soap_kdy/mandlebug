﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;




public class PlayerHealth : MonoBehaviour
{
    
    public GameObject deathEffect;

    public int curHealth;
    public int maxHealth = 5;
    




    void Start()
    {
        curHealth = maxHealth;
    
    }

    void Update()
    {
        if (curHealth > maxHealth)
        {
            curHealth = maxHealth;
        }

        if(curHealth <= 0)
        {
            Die();
            
        }
       // if (curHealth <= 0)
        //{
            //StartCoroutine(deathScreenLoad());
        //}
        
      
    }
    // trying to add a time delay so the death screen comes up few secs after player dies
   //private IEnumerator deathScreenLoad()
   // {
        //yield return new WaitForSeconds(1f);
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

   // }
    


    void Die()
    {
        Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(gameObject);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);


    }
    // player will flash green when damaged
    public void Damage( int dng)
    {
        curHealth -= dng;
        gameObject.GetComponent<Animation>().Play("flashDamage");
    }
    
}
