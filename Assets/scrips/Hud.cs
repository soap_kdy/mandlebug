﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Hud : MonoBehaviour
{
    public Sprite[] HeartSprites;
    public Image HeartUI;
    private PlayerHealth ship;

    // this is the hearts on the top of the scren and that decreese when the player takes damage
    void Start()
    {
        ship = GameObject.FindGameObjectWithTag("ship").GetComponent<PlayerHealth>();
    }


    void Update()
    {
        HeartUI.sprite = HeartSprites[ship.curHealth];
    }
}
